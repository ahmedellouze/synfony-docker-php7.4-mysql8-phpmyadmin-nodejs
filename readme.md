# Symfony

première projet sous symfony 5.4.9.

##Environnement de développement

### Pré-requi
*PHP 7.4.27

*nginx  	1.20.2

*Mysql  	8.0

*PhpMyAdmin  	5.1.1

*Node-Js   	17.4.0



Vous pouvez vérifier les pré-requis avec la commande suivante de la CLI symfony

bash

symfony chak:resuirements 

## Ajouter pack encore de symfony

compose require encore

docker-compose run node-service yarn insttal

docker-compose run  node-service yarn dev

## Ajoouter Bootstrap 5
docker-compose run  node-service yarn add bootstrap


### Ajouter de depondence boostrap

docker-compose run  node-service yarn add @popperjs/core@^2.10.2
isntaller cette depondence si vous utlisé sass
docker-compose run  node-service yarn add webpack@^5.0.0


### install twig-bundle
composer require symfony/twig-bundle
### install annotations
symfony composer req annotations
#Commande utils

####Creation d'un nouvelle Entitée
symfony console make:entity

####Preparation fichier de migration 
symfony console make:migration
####Remanté les donées dan la BDD
symfony console d:m:m
#### Création des Entitées de Test
symfony console make:unit-test

#### Lancer des tests
php bin/phpunit --testdox



 
